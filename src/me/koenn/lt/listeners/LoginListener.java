package me.koenn.lt.listeners;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.PreLoginEvent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class LoginListener implements Listener {

    public static int players = 0;
    public static int serverPlayers = 0;

    /**
     * Gets called when a player starts logging in to the server.
     * Makes sure no more than 3 players can join in one second.
     *
     * @param event PreLoginEvent instance
     */
    @EventHandler
    public void onPreLogin(PreLoginEvent event) {
        if (players < 3) {
            event.setCancelReason("Too many players logging in, please try again!");
            event.setCancelled(true);
            event.setCancelReason("Too many players logging in, please try again!");
        } else {
            players++;
        }
    }

    /**
     * Gets called when a player tries to connect to a server.
     * Makes sure no more than 3 players can connect in one second.
     *
     * @param event ServerConnectEvent instance
     */
    @EventHandler
    public void onServerConnect(ServerConnectEvent event) {
        if (serverPlayers < 3) {
            event.setCancelled(true);
            event.getPlayer().sendMessage(new TextComponent(ChatColor.RED + "Too many players logging in, please try again!"));
        } else {
            serverPlayers++;
        }
    }
}
