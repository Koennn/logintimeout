package me.koenn.lt;

import me.koenn.lt.listeners.LoginListener;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.concurrent.TimeUnit;

public class LoginTimeout extends Plugin {

    /**
     * Registers the listeners and starts the timer.
     */
    @Override
    public void onEnable() {
        this.getProxy().getPluginManager().registerListener(this, new LoginListener());
        this.getProxy().getScheduler().schedule(this, () -> LoginListener.players = 0, 0, 1, TimeUnit.SECONDS);
        this.getProxy().getScheduler().schedule(this, () -> LoginListener.serverPlayers = 0, 0, 1, TimeUnit.SECONDS);
    }
}
